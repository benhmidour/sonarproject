import { Component, OnInit } from '@angular/core';
import { fadeInAnimation, slideInOutAnimation } from 'src/app/animations';

@Component({
  selector: 'app-expert-comptable',
  templateUrl: './expert-comptable.component.html',
  styleUrls: ['./expert-comptable.component.css'],
  animations: [fadeInAnimation]
})
export class ExpertComptableComponent implements OnInit {

  liste = ['- Déclarer et payer vos contributions Formation professionnelle et Alternance',
  '- Réaliser vos demandes de prise en charge',
  '- Accéder à un large choix de formations']

  constructor() { }

  ngOnInit() {
  }

}
