import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Branche } from 'src/app/models/Branche.model';
import { Subscription } from 'rxjs';
import { RefBranchesService } from 'src/app/services/ref-branches.service';
import { UrlDataByBrancheService } from 'src/app/services/url-data-by-branche.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { fadeInAnimation } from 'src/app/animations';
declare var $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  animations: [fadeInAnimation]
})
export class SignupComponent implements OnInit {


  @Input() newUserMessageStatus: string;
  @Input() newUserMessage;

  newUserForm: FormGroup;
  branches: Branche[];
  branchesSubscription: Subscription;
  idcc: number;

  siteKey: string = environment.reCaptchaKey;

  liste = ['- Déclarer et payer vos contributions Formation professionnelle et Alternance',
    '- Réaliser vos demandes de prise en charge',
    '- Accéder à un large choix de formations']

  constructor(private refBranches: RefBranchesService,
    private urlDataByBranche: UrlDataByBrancheService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.initForm();

    this.refBranches.getBranchesListFromServer();
    this.branchesSubscription = this.refBranches.branchesSubject.subscribe(
      (branches: Branche[]) => {
        this.branches = branches;
        this.sortBranches()
      }
    );

    this.form2TooltipsHandler();
  }

  sortBranches() {
    this.branches.sort(function (a, b) {
      return a.code - b.code;
    });
  }

  initForm() {
    //Form 2
    this.newUserForm = this.formBuilder.group({
      idcc: ["", Validators.required],
      dom: ["", Validators.required],
      recaptcha: ['', Validators.required]
    })
  }

  form2TooltipsHandler() {
    let btnElt = $('#newUserSubmitBtn');
    let tooltip = $('#tooltip2');
    let idccInputElt = $('#idccInput');
    btnElt.hover(function () {

      if (idccInputElt.val() == '') {
        tooltip.attr('title', 'Veuillez entrer un code IDCC')
        tooltip.tooltip();
      } else if (idccInputElt.val() !== '') {
        tooltip.removeAttr('title')
        tooltip.removeAttr('data-original-title')
      }
      if (idccInputElt.val() !== '' && !$('#metropole').is(":checked") && !$('#dom').is(":checked")) {
        tooltip.attr('data-original-title', 'Veuillez sélectionner DOM ou métropole')
        tooltip.tooltip('show');
      }

    })
  }

  /**
   * Test si le login donné est uniquement une suite de chiffres
   * @param login le login à tester
   */

  isNumbers(login) {
    let regex = /^\d+$/;
    return regex.test(String(login).toLowerCase());
  }

  displayMessage2(message: string, status: string) {
    let messageElt = document.getElementById("newUserMessage");
    messageElt.style.display = "block";
    this.newUserMessage = message;
    this.newUserMessageStatus = status;
    setTimeout(function () {
      messageElt.style.display = "none";
    }, 3000);
  }


  onSubmit2(finished?) {
    this.idcc = this.newUserForm.get('idcc').value;
    const dom = this.newUserForm.get('dom').value;

    // Si l'idcc est un nombre de 4 chiffres
    if (this.isNumbers(this.idcc) && this.idcc.toString().length === 4) {
      this.urlDataByBranche.getDataByBrancheFromServer(this.idcc, dom).then(
        (branche: any) => {
          //Si la branche existe
          if (branche.length > 0) {
            if (branche[0].destinationUrl.toLowerCase() === 'agelink') {
              //On ouvre le modal
              $('#modal').modal('show');
            } else {
              // On redirige l'utilisateur vers la page de connexion avec l'url reçue
              window.location.href = branche[0].destinationUrl;
            }
          } else {
            this.displayMessage2("IDCC inconnu", "error");
          }
          if(finished) finished(branche);
        }
      )
    } else {
      this.displayMessage2("Veuillez entrer un IDCC valide", "error");
    }
  }

}
