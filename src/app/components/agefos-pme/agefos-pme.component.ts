import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from 'src/app/animations';

@Component({
  selector: 'app-agefos-pme',
  templateUrl: './agefos-pme.component.html',
  styleUrls: ['./agefos-pme.component.css'],
  animations: [fadeInAnimation]
})
export class AgefosPmeComponent implements OnInit {

  liste = ['- Déclarer et payer vos contributions Formation professionnelle et Alternance',
  '- Réaliser vos demandes de prise en charge',
  '- Accéder à un large choix de formations']

  constructor() { }

  ngOnInit() {
  }

}
