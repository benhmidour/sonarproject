import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { LoginComponent } from './components/login/login.component';
import { AgefosPmeComponent } from './components/agefos-pme/agefos-pme.component';
import { ExpertComptableComponent } from './components/expert-comptable/expert-comptable.component';
import { SignupComponent } from './components/signup/signup.component';
import { NouvelAdherentComponent } from './components/nouvel-adherent/nouvel-adherent.component';
import { OrganismeDeFormationComponent } from './components/organisme-de-formation/organisme-de-formation.component';
import { MentionsLegalesComponent } from './components/legal/mentions-legales/mentions-legales.component';
import { RgpdComponent } from './components/legal/rgpd/rgpd.component';
import { CookiesComponent } from './components/legal/cookies/cookies.component';
import { PhoneMaskDirective } from './directives/phone-mask.directive';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    FooterComponent,
    MenuComponent,
    LoginComponent,
    AgefosPmeComponent,
    ExpertComptableComponent,
    SignupComponent,
    NouvelAdherentComponent,
    OrganismeDeFormationComponent,
    MentionsLegalesComponent,
    RgpdComponent,
    CookiesComponent,
    PhoneMaskDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
