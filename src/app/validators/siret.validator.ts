import { AbstractControl, ValidationErrors } from "@angular/forms";

export function ValidateSiret (control: AbstractControl): ValidationErrors | null {
  let siret: any = control.value;
  siret = siret.replace(/\s/g, "");
  if (isNaN(siret) || siret.length != 14) return {'siret': true};
  let regex = /^[356]/;
  if (regex.test(siret)) {
    return null;
  }
  var bal = 0;
  var total = 0;
  for (var i = 14 - 1; i >= 0; i--) {
    var step = (siret.charCodeAt(i) - 48) * (bal + 1);
    total += step > 9 ? step - 9 : step;
    bal = 1 - bal;
  }
  return total % 10 == 0 ? null : {'siret': true};
}