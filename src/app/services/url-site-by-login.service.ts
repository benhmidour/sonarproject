import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UrlSiteByLoginService {

  APIEndpoint = environment.APIEndpoint;

  constructor(private http: HttpClient) { }

  getLogin(login: string) {
    return new Promise(
      (resolve, reject) => {
        this.http.get(this.APIEndpoint + "apis/routage/v1/knownuser/" + login).subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }




}