import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  @Input() sousTitre: string;
  @Input() liste:string[];
  constructor() { }

  ngOnInit() {
  }  

}
