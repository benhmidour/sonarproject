import { TestBed, async, getTestBed } from '@angular/core/testing';

import { UrlSiteBySiretService } from './url-site-by-siret.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('UrlSiteBySiretService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports:[HttpClientTestingModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  it('should be created', () => {
    const service: UrlSiteBySiretService = TestBed.get(UrlSiteBySiretService);
    expect(service).toBeTruthy();
  });
});
