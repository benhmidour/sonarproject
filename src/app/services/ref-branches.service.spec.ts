import { TestBed, async, getTestBed } from '@angular/core/testing';

import { RefBranchesService } from './ref-branches.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('RefBranchesService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports:[HttpClientTestingModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  it('should be created', () => {
    const service: RefBranchesService = TestBed.get(RefBranchesService);
    expect(service).toBeTruthy();
  });
});
