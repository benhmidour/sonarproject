import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AgefosPmeComponent } from './components/agefos-pme/agefos-pme.component';
import { ExpertComptableComponent } from './components/expert-comptable/expert-comptable.component';
import { SignupComponent } from './components/signup/signup.component';
import { NouvelAdherentComponent } from './components/nouvel-adherent/nouvel-adherent.component';
import { OrganismeDeFormationComponent } from './components/organisme-de-formation/organisme-de-formation.component';
import { MentionsLegalesComponent } from './components/legal/mentions-legales/mentions-legales.component';
import { RgpdComponent } from './components/legal/rgpd/rgpd.component';
import { CookiesComponent } from './components/legal/cookies/cookies.component';

export const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'nouvel-adherent', component: SignupComponent },
  { path: 'agelink', component: AgefosPmeComponent },
  { path: 'agelink-nouvel-adherent/:idcc', component: NouvelAdherentComponent },
  { path: 'organisme-de-formation', component: OrganismeDeFormationComponent },
  { path: 'expert-comptable', component: ExpertComptableComponent },
  { path: 'mentions-legales', component: MentionsLegalesComponent },
  { path: 'rgpd', component: RgpdComponent },
  { path: 'cookies', component: CookiesComponent },
	{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
