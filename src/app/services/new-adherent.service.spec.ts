import { TestBed, async, getTestBed } from '@angular/core/testing';

import { NewAdherentService } from './new-adherent.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('NewAdherentService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports:[HttpClientTestingModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  it('should be created', () => {
    const service: NewAdherentService = TestBed.get(NewAdherentService);
    expect(service).toBeTruthy();
  });
});
