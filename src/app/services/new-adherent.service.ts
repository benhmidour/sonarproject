import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { NouvelAdherent } from '../models/NouvelAdherent.model';

@Injectable({
  providedIn: 'root'
})
export class NewAdherentService {
  APIEndpoint = environment.APIEndpoint;


  constructor(private httpClient: HttpClient) { }


  saveNouvelAdherentToServer(nouvelAdherent: NouvelAdherent) {
		
		return this.httpClient.post(this.APIEndpoint + 'datalab/apis/v1/entreprise', nouvelAdherent);
	}
}
