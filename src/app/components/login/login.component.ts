import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UrlSiteByLoginService } from "src/app/services/url-site-by-login.service";
import { UrlSiteBySiretService } from "src/app/services/url-site-by-siret.service";
import { environment } from "../../../environments/environment";
import { fadeInAnimation } from 'src/app/animations';
declare var $: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  animations: [fadeInAnimation]
})
export class LoginComponent implements OnInit {
  @Input() messageStatus: string;
  @Input() formMessage;

  loginForm: FormGroup;
  siteKey: string = environment.reCaptchaKey;
  user: any;
  modalMessage: string;

  liste = [
    "- Déclarer et payer vos contributions Formation professionnelle et Alternance",
    "- Réaliser vos demandes de prise en charge",
    "- Accéder à un large choix de formations"
  ];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private urlSiteByLogin: UrlSiteByLoginService,
    private urlSiteBySiret: UrlSiteBySiretService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    // Form 1
    this.loginForm = this.formBuilder.group({
      login: ["", Validators.required],
      recaptcha: ["", Validators.required]
    });
  }

  /**
   * Test si le login donné est uniquement une suite de chiffres
   * @param login le login à tester
   */

  isNumbers(login) {
    let regex = /^\d+$/;
    return regex.test(String(login).toLowerCase());
  }

  /**
   * Vérifier si le login est un siret
   * @param login le login a vérifier
   * @return bool
   */
  isSiret(login) {
    if (isNaN(login) || login.length != 14) return false;
    let regex = /^[356]/;
    if (regex.test(login)) {
      return true;
    }
    var bal = 0;
    var total = 0;
    for (var i = 14 - 1; i >= 0; i--) {
      var step = (login.charCodeAt(i) - 48) * (bal + 1);
      total += step > 9 ? step - 9 : step;
      bal = 1 - bal;
    }
    return total % 10 == 0 ? true : false;
  }

  isEmail(email) {
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
  }

  displayMessage(message: string, status: string) {
    let messageElt = document.getElementById("formMessage");
    messageElt.style.display = "block";
    this.formMessage = message;
    this.messageStatus = status;
  }

  onSubmit1(success?) {
    let login = this.loginForm.get("login").value;

    // On échape les espaces
    login = login.replace(/\s/g, "");
    // Si le login est un nombre
    if (this.isNumbers(login)) {
      // Si le login est un siret valide
      if (this.isSiret(login)) {
        //On récupère le login avec le siret
        this.urlSiteBySiret.getSiret(login).then((siret: any) => {
          this.user = siret;
          //On envoie au routage
          this.routage(siret);
          if(success != null) success();
        });
      } else {
        this.displayMessage("Veuillez entrer un Siret valide", "error");
      }
    }
    // Si le login est une string
    else if (typeof login === "string") {
      // Validation de l'email
      if (this.isEmail(login)) {
        // Récupération du login
        this.urlSiteByLogin.getLogin(login).then((user: any) => {
          this.user = user;
          //On envoie au routage
          this.routage(user);
          if(success != null) success();
        });
      } else {
        this.displayMessage("Veuillez entrer un email valide", "error");
      }
    }
  }

  /**
   * Permet de gérer le routage de l'application en fonction de la réponse de l'api routage
   * @param login la réponse de l'api routage
   */
  routage(login: any) {
    // Si le login est inconnu
    if (login.code === "002") {
      // On affiche un message d'erreur
      this.displayMessage(login.message, "error");
    }
    // Si le login est connu
    if (login.code === "001") {
      // Si c'est un login agelink, on redirige vers la page agelink
      if (login.user.destinationUrl.toLowerCase() === "agelink") {
        this.router.navigate(["agelink"]);
      }
      // Si c'est un login Actalians, on redirige vers la page eactalians
      else {
        window.location.href = login.user.destinationUrl.toLowerCase();
      }
    }
    // Si le login est sur 2 branches, on ouvre le modal de séléction de la branche
    if (login.code === "003") {
      $("#modalLogin").modal("show");
      this.modalMessage = login.message;
    }
    // Cas d'une branche inconnue
    if (login.code === "004") {
      // On affiche un message d'erreur
      this.displayMessage(login.message, "error");
    }
  }

  /**
   * Redirige l'utilisateur en fonction du choix de la branche
   * @param branche la branche choisie
   */
  onChooseBranche(branche: any) {
    // Si c'est un login agelink, on redirige vers la page agelink
    if (branche.destinationUrl.toLowerCase() === "agelink") {
      this.router.navigate(["agelink"]);
    }
    // Si c'est un login Actalians, on redirige vers la page eactalians
    else {
      window.location.href = branche.destinationUrl.toLowerCase();
    }
  }
}
