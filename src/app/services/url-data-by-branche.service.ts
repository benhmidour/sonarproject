import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UrlDataByBrancheService {
  APIEndpoint = environment.APIEndpoint;

  constructor(private http: HttpClient) { }

  /**
   * 
   * @param brancheId l'id de la branche
   * @param dom 
   */
  getDataByBrancheFromServer(brancheId: number, dom:boolean) {
    return new Promise(
      (resolve, reject) => {
        this.http.get(this.APIEndpoint + "apis/routage/v1/knownbranch/" + brancheId + "/" + dom).subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

}
