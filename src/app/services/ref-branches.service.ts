import { Injectable } from '@angular/core';
import { Branche } from '../models/Branche.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RefBranchesService {

  branches: Branche[] = [];
  branchesSubject = new Subject<any[]>();
  APIEndpoint = environment.APIEndpoint;

  constructor(private http: HttpClient) { }

  emitBranches() {
    this.branchesSubject.next(this.branches);
  }

  getBranchesListFromServer() {
    return this.http.get<any[]>(this.APIEndpoint + 'datalab/apis/v1/listBrancheFull').subscribe(
      (response) => {
        this.branches = response;
        this.emitBranches();
      },
      (error) => {
        console.error(JSON.stringify(error));
      }
    )
  }
}
