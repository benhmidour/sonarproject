import { TestBed } from '@angular/core/testing';

import { UrlDataByBrancheService } from './url-data-by-branche.service';
import { HttpClientModule } from '@angular/common/http';

describe('UrlDataByBrancheService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: UrlDataByBrancheService = TestBed.get(UrlDataByBrancheService);
    expect(service).toBeTruthy();
  });
});
