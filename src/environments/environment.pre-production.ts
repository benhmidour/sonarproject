// Pre-prod

export const environment = {
    APIEndpoint: "https://kong-pre-production.lab.opcoep.fr/",
    reCaptchaKey: "6Le9OscUAAAAAK02UMgwaQjvqd-IkIVqSD5ujusj",
    googleAnalyticsID: 'UA-163210383-2',
    production: true
  };