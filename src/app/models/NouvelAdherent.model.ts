export class NouvelAdherent {
    siret: number;
      naf: string;
      idcc: number;
      libelle: string;
      raisonSociale: string;
      numeroVoie: number;
      extensionVoie: string;
      typeVoie: string;
      nomVoie: string;
      complementVoie: string;
      complementVoie2: string;
      ville: string;
      codePostal: number;
      formeJuridique: string;
      assujetissementTva: boolean;
      masseSalariale: string;
      effectifMoyenAnnuel: string;
      nom: string;
      prenom: string;
      email: string;
      telephone: string;
      rgpd: boolean;
}