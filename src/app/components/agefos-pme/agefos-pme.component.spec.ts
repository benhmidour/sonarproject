import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgefosPmeComponent } from './agefos-pme.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxCaptchaModule } from 'ngx-captcha';
import { MockModule } from 'ng-mocks';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from 'src/app/app.component';
import { AccueilComponent } from '../accueil/accueil.component';

describe('AgefosPmeComponent', () => {
  let component: AgefosPmeComponent;
  let fixture: ComponentFixture<AgefosPmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgefosPmeComponent, AccueilComponent ],
      imports:[FormsModule, ReactiveFormsModule, MockModule(NgxCaptchaModule)
        , RouterTestingModule.withRoutes([]), HttpClientTestingModule, BrowserAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgefosPmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
