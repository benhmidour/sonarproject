import { TestBed, async, getTestBed } from '@angular/core/testing';

import { UrlSiteByLoginService } from './url-site-by-login.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('UrlSiteByLoginService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports:[HttpClientTestingModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  it('should be created', () => {
    const service: UrlSiteByLoginService = TestBed.get(UrlSiteByLoginService);
    expect(service).toBeTruthy();
  });
});
