import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from 'src/app/animations';

@Component({
  selector: 'app-organisme-de-formation',
  templateUrl: './organisme-de-formation.component.html',
  styleUrls: ['./organisme-de-formation.component.css'],
  animations: [fadeInAnimation]
})
export class OrganismeDeFormationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
