import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertComptableComponent } from './expert-comptable.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { AccueilComponent } from '../accueil/accueil.component';

describe('ExpertComptableComponent', () => {
  let component: ExpertComptableComponent;
  let fixture: ComponentFixture<ExpertComptableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertComptableComponent, AccueilComponent ],
      imports:[FormsModule, ReactiveFormsModule, MockModule(NgxCaptchaModule)
        , RouterTestingModule.withRoutes([]), HttpClientTestingModule, BrowserAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertComptableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
