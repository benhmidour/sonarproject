import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { NouvelAdherentComponent } from './nouvel-adherent.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { AccueilComponent } from '../accueil/accueil.component';
import { environment } from 'src/environments/environment';

describe('NouvelAdherentComponent', () => {
  let component: NouvelAdherentComponent;
  let fixture: ComponentFixture<NouvelAdherentComponent>;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  let injector: TestBed;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NouvelAdherentComponent, AccueilComponent ],
      imports:[FormsModule, ReactiveFormsModule, MockModule(NgxCaptchaModule)
        , RouterTestingModule.withRoutes([]), HttpClientTestingModule, BrowserAnimationsModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NouvelAdherentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when siret is empty', () => {
    component.contactForm.get('siret').setValue('');
    component.onNextTab();
    expect(component.contactForm.controls.siret.invalid).toBeTruthy();
  });

  it('should be invalid when naf is empty', () => {
    component.contactForm.get('naf').setValue('');
    expect(component.contactForm.controls.naf.invalid).toBeTruthy();
  });

  xit('should be invalid when idcc is empty', () => {
    component.contactForm.get('idcc').setValue('');
    expect(component.contactForm.controls.idcc.invalid).toBeTruthy();
  });

  it('should be invalid when raisonSociale is empty', () => {
    component.contactForm.get('raisonSociale').setValue('');
    expect(component.contactForm.controls.raisonSociale.invalid).toBeTruthy();
  });

  it('should be invalid when numeroVoie is empty', () => {
    component.contactForm.get('numeroVoie').setValue('');
    expect(component.contactForm.controls.numeroVoie.invalid).toBeTruthy();
  });

  it('should be invalid when typeVoie is empty', () => {
    component.contactForm.get('typeVoie').setValue('');
    expect(component.contactForm.controls.typeVoie.invalid).toBeTruthy();
  });

  it('should be invalid when nomVoie is empty', () => {
    component.contactForm.get('nomVoie').setValue('');
    expect(component.contactForm.controls.nomVoie.invalid).toBeTruthy();
  });

  it('should be invalid when ville is empty', () => {
    component.contactForm.get('ville').setValue('');
    expect(component.contactForm.controls.ville.invalid).toBeTruthy();
  });

  it('should be invalid when codePostal is empty', () => {
    component.contactForm.get('codePostal').setValue('');
    expect(component.contactForm.controls.codePostal.invalid).toBeTruthy();
  });

  it('should be invalid when formeJuridique is empty', () => {
    component.contactForm.get('formeJuridique').setValue('');
    expect(component.contactForm.controls.formeJuridique.invalid).toBeTruthy();
  });

  it('should be invalid when assujetissementTva is empty', () => {
    component.contactForm.get('assujetissementTva').setValue('');
    expect(component.contactForm.controls.assujetissementTva.invalid).toBeTruthy();
  });

  it('should be invalid when masseSalariale is empty', () => {
    component.contactForm.get('masseSalariale').setValue('');
    expect(component.contactForm.controls.masseSalariale.invalid).toBeTruthy();
  });

  it('should be invalid when effectifMoyenAnnuel is empty', () => {
    component.contactForm.get('effectifMoyenAnnuel').setValue('');
    component.onNextTab2();
    expect(component.contactForm.controls.masseSalariale.invalid).toBeTruthy();
  });

  it('should be invalid when siret content is incorrect', () => {
    component.contactForm.get('siret').setValue('qergqer');
    expect(component.contactForm.controls.siret.invalid).toBeTruthy();
  });

  it('should be invalid when naf content is incorrect', () => {
    component.contactForm.get('naf').setValue('qregqerg');
    expect(component.contactForm.controls.naf.invalid).toBeTruthy();
  });

  xit('should be invalid when idcc content is incorrect', () => {
    component.contactForm.get('idcc').setValue('qregqerg');
    expect(component.contactForm.controls.idcc.invalid).toBeTruthy();
  });

  it('should be invalid when numeroVoie content is incorrect', () => {
    component.contactForm.get('numeroVoie').setValue('&&');
    expect(component.contactForm.controls.numeroVoie.invalid).toBeTruthy();
  });

  it('should be invalid when codePostal content is incorrect', () => {
    component.contactForm.get('codePostal').setValue('qregqerg');
    expect(component.contactForm.controls.codePostal.invalid).toBeTruthy();
  });

  it('should be invalid when masseSalariale is empty', () => {
    component.contactForm.get('masseSalariale').setValue('sergqerg');
    expect(component.contactForm.controls.masseSalariale.invalid).toBeTruthy();
  });

  it('should be invalid when effectifMoyenAnnuel is empty', () => {
    component.contactForm.get('effectifMoyenAnnuel').setValue('qdgqgqdrg');
    expect(component.contactForm.controls.masseSalariale.invalid).toBeTruthy();
  });

  it('should be invalid when siret is already used in routage', () => {
    let dummyResponse = {code: '005'};
    let siret = '42216671000018';
    component.contactForm.get('siret').setValue(siret);
    component.onCheckSiret((code1)=>{
      expect(code1).toEqual("005");
    });

    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownenterprise/" + siret);
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should be invalid when siret is already used in datalab', () => {
    let dummyResponse1 = {code: '006'};
    let dummyResponse2 = {code: '005'};
    let siret = '42216671000018';
    component.contactForm.get('siret').setValue(siret);
    component.onCheckSiret((code1)=>{
      expect(code1).toEqual("006");
      const req2 = httpMock.expectOne(APIEndpoint + "datalab/apis/v1/siret?siretParam=" + siret);
      expect(req2.request.method).toBe("GET");
      req2.flush(dummyResponse2);
    },
    (code2)=>{
      expect(code2).toEqual("005");
    });

    const req1 = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownenterprise/" + siret);
    expect(req1.request.method).toBe("GET");
    req1.flush(dummyResponse1);
  });

  it('should be valid when siret is never used', () => {
    let dummyResponse1 = {code: '006'};
    let dummyResponse2 = {code: '006'};
    let siret = '42216671000018';
    component.contactForm.get('siret').setValue(siret);
    component.onCheckSiret((code1)=>{
      expect(code1).toEqual("006");
      const req2 = httpMock.expectOne(APIEndpoint + "datalab/apis/v1/siret?siretParam=" + siret);
      expect(req2.request.method).toBe("GET");
      req2.flush(dummyResponse2);
    },
    (code2)=>{
      expect(code2).toEqual("006");
    });

    const req1 = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownenterprise/" + siret);
    expect(req1.request.method).toBe("GET");
    req1.flush(dummyResponse1);
  });

  it('should show confirmation when submit', () => {
    let dummyResponse1 = {code: '006'};
    
    component.contactForm.get('siret').setValue('42216671000018');
    component.contactForm.get('naf').setValue('A1234');
    component.contactForm.get('idcc').setValue('ABCD');
    component.contactForm.get('raisonSociale').setValue('dgqgqdg');
    component.contactForm.get('numeroVoie').setValue('1');
    component.contactForm.get('typeVoie').setValue('BIS');
    component.contactForm.get('nomVoie').setValue('qrgq');
    component.contactForm.get('ville').setValue('qrdgqdg');
    component.contactForm.get('codePostal').setValue('12222');
    component.contactForm.get('formeJuridique').setValue('dfg');
    component.contactForm.get('assujetissementTva').setValue('qdg');
    component.contactForm.get('masseSalariale').setValue('2342$');
    component.contactForm.get('effectifMoyenAnnuel').setValue('2342$');
    
    component.contactForm.get('nom').setValue('sdfg');
    component.contactForm.get('prenom').setValue('qdrg');
    component.contactForm.get('email').setValue('fff@ff.ff');
    component.contactForm.get('telephone').setValue('06 52 42 42 42');
    component.contactForm.get('rgpd').setValue(true);
    component.contactForm.get('recaptcha').setValue('sergsvgqsf');

    component.onSubmit(()=>{
      expect(/^(\w|\W)*(prise en compte)(\w|\W)*/.test(component.modalMessage)).toBeTruthy();
    });

    const req1 = httpMock.expectOne(APIEndpoint + "datalab/apis/v1/entreprise");
    expect(req1.request.method).toBe("POST");
    req1.flush(dummyResponse1);
  });
  
});