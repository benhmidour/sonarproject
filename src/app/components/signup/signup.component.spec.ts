import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { SignupComponent } from './signup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { MockModule } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { AccueilComponent } from '../accueil/accueil.component';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupComponent, AccueilComponent ],
      imports:[FormsModule, ReactiveFormsModule, MockModule(NgxCaptchaModule)
        , RouterTestingModule.withRoutes([]), HttpClientTestingModule, BrowserAnimationsModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    router = injector.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return false when login is not number', () => {
    expect(component.isNumbers('aexaed')).toBeFalsy();
  });
  
  it('should return true when login is number', () => {
    expect(component.isNumbers('1234')).toBeTruthy();
  });

  it('should call getDataByBranche service when submit correct idcc', () => {
    let dummyResponse = [{destinationUrl: 'agelink'}];
    let brancheId = 1234;
    let dom = '';

    component.newUserForm.get('idcc').setValue(brancheId);
    component.newUserForm.get('dom').setValue(dom);
    component.onSubmit2((branche)=>{
      expect(branche).toEqual(dummyResponse);
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownbranch/" + brancheId + "/" + dom);
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should show message when submit correct idcc and response is empty', () => {
    let dummyResponse = [];
    let brancheId = 1234;
    let dom = '';

    component.newUserForm.get('idcc').setValue(brancheId);
    component.newUserForm.get('dom').setValue(dom);
    component.onSubmit2((branche)=>{
      expect(component.newUserMessage).toEqual("IDCC inconnu");
      expect(component.newUserMessageStatus).toEqual("error");
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownbranch/" + brancheId + "/" + dom);
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should show message when submit correct idcc and response is empty', () => {
    let brancheId = 12345;
    let dom = '';

    component.newUserForm.get('idcc').setValue(brancheId);
    component.newUserForm.get('dom').setValue(dom);
    component.onSubmit2();
  
    expect(component.newUserMessage).toEqual("Veuillez entrer un IDCC valide");
    expect(component.newUserMessageStatus).toEqual("error");
  });
});
