import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "./../../environments/environment";
import { resolve } from "url";

@Injectable({
  providedIn: "root"
})
export class SiretService {
  APIEndpoint = environment.APIEndpoint;

  constructor(private http: HttpClient) {}

  checkSiretRoutage(siret: any) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.APIEndpoint + "apis/routage/v1/knownenterprise/" + siret)
        .subscribe(
          response => {
            resolve(response);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  checkSiretDatalab(siret: any) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.APIEndpoint + "datalab/apis/v1/siret?siretParam=" + siret)
        .subscribe(
          response => {
            resolve(response);
          },
          error => {
            reject(error);
          }
        );
    });
  }
}
