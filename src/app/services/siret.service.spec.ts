import { TestBed, async, getTestBed } from '@angular/core/testing';

import { SiretService } from './siret.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('SiretService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports:[HttpClientTestingModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  it('should be created', () => {
    const service: SiretService = TestBed.get(SiretService);
    expect(service).toBeTruthy();
  });
});
