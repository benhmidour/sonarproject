import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { environment } from './../environments/environment';
declare var gtag: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  googleAnalyticsID = environment.googleAnalyticsID;

  constructor(private router: Router) {
    const navEndEvents =  router.events.pipe(
      filter(event => event instanceof NavigationEnd));
    navEndEvents.subscribe((event: NavigationEnd) => {
      gtag('config', this.googleAnalyticsID, {
        'page_path': event.urlAfterRedirects
      })
    });
  }
}
