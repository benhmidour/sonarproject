import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganismeDeFormationComponent } from './organisme-de-formation.component';
import { AccueilComponent } from '../accueil/accueil.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxCaptchaModule } from 'ngx-captcha';
import { MockModule } from 'ng-mocks';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('OrganismeDeFormationComponent', () => {
  let component: OrganismeDeFormationComponent;
  let fixture: ComponentFixture<OrganismeDeFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganismeDeFormationComponent, AccueilComponent ],
      imports:[FormsModule, ReactiveFormsModule, MockModule(NgxCaptchaModule)
        , RouterTestingModule.withRoutes([]), HttpClientTestingModule, BrowserAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganismeDeFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
