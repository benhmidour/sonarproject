import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UrlSiteBySiretService {
  APIEndpoint = environment.APIEndpoint;

  constructor(private http: HttpClient) { }

  getSiret(siret: number) {
    return new Promise(
      (resolve, reject) => {
        this.http.get(this.APIEndpoint + "apis/routage/v1/knownuser/" + siret).subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }



}
