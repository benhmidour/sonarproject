import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AccueilComponent } from '../accueil/accueil.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockModule } from 'ng-mocks';
import { environment } from 'src/environments/environment';
import { AgefosPmeComponent } from '../agefos-pme/agefos-pme.component';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let APIEndpoint = environment.APIEndpoint;
  let router: Router;

  beforeEach(async(() => {
    
    TestBed.configureTestingModule({
      declarations: [ LoginComponent, AccueilComponent, AgefosPmeComponent ],
      imports:[FormsModule, ReactiveFormsModule, MockModule(NgxCaptchaModule)
        , RouterTestingModule.withRoutes([{ path: 'agelink', component: AgefosPmeComponent }]), HttpClientTestingModule, BrowserAnimationsModule]
    })
    .compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    router = injector.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return true when login 123456', () => {
    expect(component.isNumbers('123456')).toBeTruthy();
  });

  it('should validate the siret', () => {
    expect(component.isSiret('123456')).toBeFalsy();
    expect(component.isSiret(NaN)).toBeFalsy();
    expect(component.isSiret('34543454345345')).toBeTruthy();
    expect(component.isSiret('42216671000018')).toBeTruthy();
    expect(component.isSiret('42216671000019')).toBeFalsy();
  });

  it('should validate the login', () => {
    expect(component.isEmail('xxxx@xxxx')).toBeFalsy();
    expect(component.isEmail('xxxxxxxx')).toBeFalsy();
    expect(component.isEmail('xxxxxxxx')).toBeFalsy();
    expect(component.isEmail('xxx@xxx.xx')).toBeTruthy();
  });

  it('should diplay message xxxxx', () => {
    component.displayMessage('xxxxx', 'xx');
    expect(component.formMessage).toEqual('xxxxx');
    expect(component.messageStatus).toEqual('xx');
  });

  it('should submit1 siret connu agelink', () => {
    let dummyResponse = {code:'001', message: 'message', user: {destinationUrl: 'agelink'}};
    component.loginForm.get('login').setValue('42216671000018');
    component.onSubmit1(()=>{
      expect(component.user).toEqual(dummyResponse);
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownuser/42216671000018");
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should show form message when submit1 siret connu agelink', () => {
    let dummyResponse = {code:'002', message: 'message', user: {destinationUrl: 'agelink'}};
    component.loginForm.get('login').setValue('42216671000018');
    component.onSubmit1(()=>{
      expect(component.formMessage).toEqual(dummyResponse.message);
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownuser/42216671000018");
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should show modal message when submit1 siret connu agelink', () => {
    let dummyResponse = {code:'003', message: 'message', user: {destinationUrl: 'agelink'}};
    component.loginForm.get('login').setValue('42216671000018');
    component.onSubmit1(()=>{
      expect(component.modalMessage).toEqual(dummyResponse.message);
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownuser/42216671000018");
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should show form message when submit1 siret connu agelink', () => {
    let dummyResponse = {code:'004', message: 'message', user: {destinationUrl: 'agelink'}};
    component.loginForm.get('login').setValue('42216671000018');
    component.onSubmit1(()=>{
      expect(component.formMessage).toEqual(dummyResponse.message);
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownuser/42216671000018");
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

  it('should submit1 login connu agelink', () => {
    let dummyResponse = {code:'001', message: 'message', user: {destinationUrl: 'agelink'}};
    component.loginForm.get('login').setValue('xx@xxx.xx');
    component.onSubmit1(()=>{
      expect(component.user).toEqual(dummyResponse);
    });
    
    const req = httpMock.expectOne(APIEndpoint + "apis/routage/v1/knownuser/xx@xxx.xx");
    expect(req.request.method).toBe("GET");
    req.flush(dummyResponse);
  });

});
