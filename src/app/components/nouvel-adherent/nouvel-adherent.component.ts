import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { ValidateSiret } from 'src/app/validators/siret.validator';
import { NouvelAdherent } from 'src/app/models/NouvelAdherent.model';
import { NewAdherentService } from 'src/app/services/new-adherent.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SiretService } from 'src/app/services/siret.service';
import { fadeInAnimation } from 'src/app/animations';
declare var $ :any;

@Component({
  selector: 'app-nouvel-adherent',
  templateUrl: './nouvel-adherent.component.html',
  styleUrls: ['./nouvel-adherent.component.css'],
  animations: [fadeInAnimation]
})
export class NouvelAdherentComponent implements OnInit {

  siteKey: string = environment.reCaptchaKey;
  contactForm: FormGroup;
  captchaError: boolean;
  idcc: number;
  modalMessage: string;

  constructor(private formBuilder: FormBuilder,
              private newAdherentService: NewAdherentService,
              private router: Router,
              private route: ActivatedRoute,
              private siretService: SiretService) { }

  ngOnInit() {
    this.idcc = this.route.snapshot.params['idcc'];
    this.initForm();
  }

  onNextTab() {
    if(this.contactForm.controls.siret.invalid ||
       this.contactForm.controls.naf.invalid ||
       this.contactForm.controls.idcc.invalid ||
       this.contactForm.controls.raisonSociale.invalid ||
       this.contactForm.controls.numeroVoie.invalid ||
       this.contactForm.controls.typeVoie.invalid ||
       this.contactForm.controls.nomVoie.invalid ||
       this.contactForm.controls.ville.invalid ||
       this.contactForm.controls.codePostal.invalid) {
        this.contactForm.controls.siret.markAsTouched();
       this.contactForm.controls.naf.markAsTouched();
       this.contactForm.controls.idcc.markAsTouched();
       this.contactForm.controls.raisonSociale.markAsTouched();
       this.contactForm.controls.numeroVoie.markAsTouched(); 
       this.contactForm.controls.typeVoie.markAsTouched();
       this.contactForm.controls.nomVoie.markAsTouched();
       this.contactForm.controls.ville.markAsTouched();
       this.contactForm.controls.codePostal.markAsTouched();
       } else {
        $('.nav-tabs > .active').next('button').trigger('click');
       }
    
  }
  onNextTab2() {
    if(this.contactForm.controls.siret.invalid ||
       this.contactForm.controls.naf.invalid ||
       this.contactForm.controls.idcc.invalid ||
       this.contactForm.controls.raisonSociale.invalid ||
       this.contactForm.controls.numeroVoie.invalid ||
       this.contactForm.controls.typeVoie.invalid ||
       this.contactForm.controls.nomVoie.invalid ||
       this.contactForm.controls.ville.invalid ||
       this.contactForm.controls.codePostal.invalid ||
       this.contactForm.controls.formeJuridique.invalid ||
       this.contactForm.controls.assujetissementTva.invalid ||
       this.contactForm.controls.masseSalariale.invalid ||
       this.contactForm.controls.effectifMoyenAnnuel.invalid) {
        this.contactForm.controls.siret.markAsTouched();
        this.contactForm.controls.naf.markAsTouched();
        this.contactForm.controls.idcc.markAsTouched();
        this.contactForm.controls.raisonSociale.markAsTouched();
        this.contactForm.controls.numeroVoie.markAsTouched();
        this.contactForm.controls.typeVoie.markAsTouched();
        this.contactForm.controls.nomVoie.markAsTouched();
        this.contactForm.controls.ville.markAsTouched();
        this.contactForm.controls.codePostal.markAsTouched();
        this.contactForm.controls.formeJuridique.markAsTouched();
        this.contactForm.controls.assujetissementTva.markAsTouched();
        this.contactForm.controls.masseSalariale.markAsTouched();
       } else {
        $('.nav-tabs > .active').next('button').trigger('click');
       }
    
  }

  onPreviousTab() {
    $('.nav-tabs > .active').prev('a').trigger('click');
  }

  initForm() {
    this.contactForm = this.formBuilder.group({
      siret: ['', [Validators.required, ValidateSiret]],
      naf: ['', [Validators.required, Validators.pattern(/^([a-zA-Z]\d{4}|\d[a-zA-Z]\d{3}|\d{2}[a-zA-Z]\d{2}|\d{3}[a-zA-Z]\d|\d{4}[a-zA-Z])$/)]],
      idcc: [{value: this.idcc, disabled: true}, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
      libelle: [''],
      raisonSociale: ['', Validators.required],
      numeroVoie: ['', [Validators.required,  Validators.pattern(/^([a-zA-Z]{0,1}\d{0,4}|\d{0,1}[a-zA-Z]{0,1}\d{0,3}|\d{0,2}[a-zA-Z]{0,1}\d{0,2}|\d{0,3}[a-zA-Z]{0,1}\d{0,1}|\d{0,4}[a-zA-Z]{0,1})$/)]],
      extensionVoie: [''],
      typeVoie: ['', Validators.required],
      nomVoie: ['', Validators.required],
      complementVoie: [''],
      complementVoie2: [''],
      ville: ['', Validators.required],
      codePostal: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
      formeJuridique: ['', Validators.required],
      assujetissementTva: ['', Validators.required],
      masseSalariale: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      effectifMoyenAnnuel: ['', Validators.pattern(/^\d+$/)],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      telephone: ['', [Validators.required, Validators.pattern(/^[0-9]{2}\s+[0-9]{2}\s+[0-9]{2}\s+[0-9]{2}\s+[0-9]{2}$/)]],
      rgpd: ['', Validators.requiredTrue],
      recaptcha: ['', Validators.required]
    });
  }

  /**
   * Valide que le siret n'a pas déjà été entré dans le formualire
   */
  onCheckSiret(finished1?, finished2?) {
    if(this.contactForm.controls.siret.valid) {
      let siretElt = <HTMLInputElement>document.getElementById('siret-input');
      let siret = siretElt.value.replace(/\s/g, "");
      this.siretService.checkSiretRoutage(siret).then(
        (response: any) => {
          if(response.code === "005") {
            $('#modal').modal('show');
          };
          if(response.code === "006") {
            this.siretService.checkSiretDatalab(siret).then(
              (response: any) => 
              {
                if(finished2) finished2(response.code);
                if(response.code === "005") {
                  $('#modal2').modal('show');
                };
                if(response.code === "006") {
                  return;
                };
              },
              (error) => {
                console.error(error);
              }
            );
          };
          if(finished1) finished1(response.code);
        },
        (error) => {
          console.error(error);
        }
      )
    };  
  }

  onSubmit(finished?) {
    if( this.contactForm.controls.nom.invalid ||
        this.contactForm.controls.prenom.invalid ||
        this.contactForm.controls.email.invalid ||
        this.contactForm.controls.telephone.invalid ||
        this.contactForm.controls.rgpd.invalid ||
        this.contactForm.controls.recaptcha.invalid ) {
          this.contactForm.controls.nom.markAsTouched();
          this.contactForm.controls.prenom.markAsTouched();
          this.contactForm.controls.email.markAsTouched();
          this.contactForm.controls.telephone.markAsTouched();
          this.contactForm.controls.rgpd.markAsTouched();
          this.contactForm.controls.recaptcha.invalid ? this.captchaError = true : this.captchaError = false;
    } else {
      const siret = this.contactForm.get('siret').value.replace(/\s/g, "");
      const naf = this.contactForm.get('naf').value;
      const idcc = this.contactForm.get('idcc').value;
      const libelle = this.contactForm.get('libelle').value;
      const raisonSociale = this.contactForm.get('raisonSociale').value;
      const numeroVoie = this.contactForm.get('numeroVoie').value;
      const extensionVoie = this.contactForm.get('extensionVoie').value;
      const typeVoie = this.contactForm.get('typeVoie').value;
      const nomVoie = this.contactForm.get('nomVoie').value;
      const complementVoie = this.contactForm.get('complementVoie').value;
      const complementVoie2 = this.contactForm.get('complementVoie2').value;
      const ville = this.contactForm.get('ville').value;
      const codePostal = this.contactForm.get('codePostal').value;
      const formeJuridique = this.contactForm.get('formeJuridique').value;
      const assujetissementTva = this.contactForm.get('assujetissementTva').value ==='true'? true : false;
      const masseSalariale = this.contactForm.get('masseSalariale').value;
      const effectifMoyenAnnuel = this.contactForm.get('effectifMoyenAnnuel').value;
      const nom = this.contactForm.get('nom').value;
      const prenom = this.contactForm.get('prenom').value;
      const email = this.contactForm.get('email').value;
      const telephone = this.contactForm.get('telephone').value;
      const rgpd = this.contactForm.get('rgpd').value;

      const nouvelAdherent = new NouvelAdherent;
      nouvelAdherent.siret = siret;
      nouvelAdherent.naf = naf;
      nouvelAdherent.idcc = idcc;
      nouvelAdherent.libelle = libelle;
      nouvelAdherent.raisonSociale = raisonSociale;
      nouvelAdherent.numeroVoie = numeroVoie;
      nouvelAdherent.extensionVoie = extensionVoie;
      nouvelAdherent.typeVoie = typeVoie;
      nouvelAdherent.nomVoie = nomVoie;
      nouvelAdherent.complementVoie = complementVoie;
      nouvelAdherent.complementVoie2 = complementVoie2;
      nouvelAdherent.ville = ville;
      nouvelAdherent.codePostal = codePostal;
      nouvelAdherent.formeJuridique = formeJuridique;
      nouvelAdherent.assujetissementTva = assujetissementTva;
      nouvelAdherent.masseSalariale = masseSalariale;
      nouvelAdherent.effectifMoyenAnnuel = effectifMoyenAnnuel;
      nouvelAdherent.nom = nom;
      nouvelAdherent.prenom = prenom;
      nouvelAdherent.email = email;
      nouvelAdherent.telephone = telephone;
      nouvelAdherent.rgpd = rgpd;

      // Envoi du formulaire à l'API via le service
      this.newAdherentService.saveNouvelAdherentToServer(nouvelAdherent).subscribe(
        () => {
          this.modalMessage = '';
          this.modalMessage = 'Votre demande a bien été prise en compte, nous reviendrons vers vous dans les meilleurs délais.';
          $('#modal3').modal('show');          
          if(finished) finished();
          this.contactForm.reset();
        },
        (error) => {
          this.modalMessage='';
          this.modalMessage = 'Erreur d\'enregistrement du formulaire: ' + error.error.message;
          $('#modal3').modal('show');
          console.error(JSON.stringify(error));
          if(finished) finished();
        }
      );
    }
  }

}